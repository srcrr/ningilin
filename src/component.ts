import { Request } from "express";
import { request } from "http";

type Hook = (req : Request, res : Response) => Promise<void>;
type InfoHook = () => string;
type ErrorHandler = (err : Error, req : Request, res : Response) => Promise<void>;
type Callable = Hook | InfoHook | ErrorHandler;

const HOOK_STUB = (req : Request, res : Response) : Promise<void> => { return null };
const ERROR_HANDLER_STUB = <ErrorHandler> (err : Error, req : Request, res : Response) : Promise<void> => { return null };
