export enum CrudAction {
    CREATE_ONE,
    CREATE_MANY,
    READ_ONE,
    READ_MANY,
    UPDATE_ONE,
    UPDATE_MANY,
    DELETE_ONE,
    DELETE_MANY,
}

export class MethodMap extends Map<CrudAction, string> {
    constructor(keys : CrudAction [], values : string  []) {
        super();
        let l = keys.length;
        if (l !== values.length) {
            throw new RangeError(`length of keys (${keys.length}) != length of values (${values.length})`);
        }
        for (let i = 0; i < l; ++i){
            this.set(keys[i], values[i]);
        }
    }

    static assign(destination : MethodMap, source : MethodMap) {
        for (let k in (source.keys() as Iterable<CrudAction>)) {
            if (k) {
                destination.set((k as unknown as CrudAction),
                                source.get(k as unknown as CrudAction));
            }
        }
    }
}