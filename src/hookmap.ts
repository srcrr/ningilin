import {Request, Response} from "express";
import stringify = require("json-stringify-safe");

export type Hook = (req : Request, res : Response) => Promise<void>;
export type InfoHook = () => any;
export type ErrorHandler = (err : Error, req : Request, res : Response, status?: any) => Promise<void>;
export type NextFunc = (() => void);
export type ExpressHook = (req : Request, res : Response, next : NextFunc) => Promise<any>;
export type Callable = (Hook | InfoHook | ErrorHandler);

export class HookMap extends Map<string, Callable> {
    constructor(callables? : Callable [], callablesObj? : object) {
        super();
        if (callables) {
            callables.forEach( (callable : Callable) => {
                console.log(`Setting ${callable.name} to ${callable}`);
                this.set(callable.name, callable);
            })
        }
        if (callablesObj) {
            Object.keys(callablesObj).forEach((key : string,
                                               index : number,
                                               arr : string []) => {
                let value = Object.values(callablesObj)[index];
                console.debug(`Setting item ${index} (key=${key}) to ${value}`);
                this.set(key, value);
            });
        }
    }
    static assign(destination : HookMap, source : HookMap) {
        for (let key in (source.keys() as IterableIterator<string>)) {
            if (key) {
                destination.set(key, source.get(key));
            }
        }
    }
}