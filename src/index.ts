import { json, Request, Response, Router, IRoute } from "express";
import { CREATED, INTERNAL_SERVER_ERROR, NOT_ACCEPTABLE, NOT_FOUND, OK } from "http-status-codes";
import { Collection, connect as mongoose_connect, disconnect as mongoose_disconnect, Document, model, Model, Mongoose, Schema } from "mongoose";
import { ExpressHook, Hook, NextFunc, Callable } from "./hookmap";
import { CrudAction } from "./methodmap";
import bodyParser = require("body-parser");
import stringify from "json-stringify-safe";
import chalk from "chalk";
import { ObjectId } from "mongodb";
import stackTrace from "stack-trace";
import { CallSite } from "callsite";

enum Action {
    CREATE_ONE = "createOne",
    CREATE_MANY = "createMany",
    READ_ONE = "readOne",
    READ_MANY = "readMany",
    UPDATE_ONE = "updateOne",
    UPDATE_MANY = "updateMany",
    DELETE_ONE = "deleteOne",
    DELETE_MANY = "deleteMany",
}

const jsonParser = bodyParser.json();

interface CallbackDict{
    funcName : string;
    func : any;
}

function copy(obj : any) {
    return JSON.parse(JSON.stringify(obj));
}

const HOOK_STUB = (req : Request, res : Response) : Promise<void> => { return null; };

export class NingilinModel {

    protected _path : string;
    protected _name : string;
    protected _model : Model<Document>;
    protected _conn : Mongoose;
    protected _collection: Collection;

    protected beforeConnect = HOOK_STUB;
    protected afterConnect = HOOK_STUB;

    protected beforeCreateOne = HOOK_STUB;
    protected afterCreateOne = HOOK_STUB;
    protected beforeCreateMany = HOOK_STUB;
    protected afterCreateMany = HOOK_STUB;

    protected beforeReadOne = HOOK_STUB;
    protected afterReadOne = HOOK_STUB;

    protected beforeUpdateOne = HOOK_STUB;
    protected afterUpdateOne = HOOK_STUB;

    protected beforeDeleteOne = HOOK_STUB;
    protected afterDeleteOne = HOOK_STUB;

    protected beforeDeleteMany = HOOK_STUB;
    protected afterDeleteMany = HOOK_STUB;

    protected beforeReadMany = HOOK_STUB;
    protected afterReadMany = HOOK_STUB;

    protected beforeDisconnect = HOOK_STUB;
    protected afterDisconnect = HOOK_STUB;

    public getPath = () : string => { return "/" + this._path };
    public getPluralPath = () : string => { return this.getPath() + "s" };

    public getConnection = () : Mongoose => { return this._conn; }

    // Defined Hooks

    /**
     * Generic error handler.
     *
     * @param {Error} err The error that was sent.
     * @param {Request} req Express request
     * @param {Response} res Express response.
     * @param {HttpStatus} status Additional HTTP status, if not already set in request. Recommended.
     *
     * @returns {Promsie<void>}
     */
    protected handleError = async (err : Error, req : Request, res : Response, status?: number) : Promise<void> => {
        console.error(`handleError(${err.message}, ${req}, ${res}, ${status})`);
        if ((!res.statusCode || res.statusCode <= 300) && status) {
            res.status(status);
        } else if ((!res.statusCode || res.statusCode <= 300) && !status) {
            res.status(INTERNAL_SERVER_ERROR);
            console.warn(`Consider adding 'res.status' in error handler.`);
        }

        if (res.statusCode >= INTERNAL_SERVER_ERROR) {
            console.error("========== SERVER ERROR ==========");
            stackTrace.parse(err).forEach( (callsite : CallSite) => {
                console.error(`${callsite.getFileName()}:${callsite.getLineNumber()}`)
            });
            console.error(err.message);
            console.error("==================================");
        }

        res.send();

        return;
    }

    /**
     * Perform a connection to the MongoDB Server.
     *
     * This will set the `_conn` property to the mongoose connection. This is not very
     * necessary unless anyone using this library wants to modify the active connection in
     * some way with the hooks.
     *
     * @param {Request} req Express request (for pre- and post-hooks)
     * @param {Response} res Express response.
     *
     * @return {Promise<void>}
     */
    protected connect = async (req : Request, res : Response) : Promise<null> => {
        try {
            console.log(`# connect`);
            await this.beforeConnect(req, res);
            let opts = process.env.MONGODB_OPTIONS || {}
            this._conn = await mongoose_connect(process.env.MONGODB_URL, opts);
            await this.afterConnect(req, res);
        } catch (err) {
            await this.handleError(err, req, res, INTERNAL_SERVER_ERROR);
        }

        this._conn = null;

        return null;
    }

    /**
     * Disconnect from the mongoose database.
     *
     * @param {Request} req Express request (for pre- and post-hooks)
     * @param {Response} res Express response.
     *
     * @return {Promise<void>}
     */
    protected disconnect = async (req : Request, res : Response) : Promise<null> => {
        try {
            console.log(`# disconnect`);
            await this.beforeDisconnect(req, res);
            await mongoose_disconnect();
            await this.afterDisconnect(req, res);
        } catch (err) {
            await this.handleError(err, req, res, INTERNAL_SERVER_ERROR);
        }

        this._conn = null;

        return null;
    }

    /**
     * Create a single instance of the model.
     *
     * @param {Request} req Express request (for pre- and post-hooks)
     * <pre>
     *  path: /<path>
     *  method: POST
     * </pre>
     *
     * @param {Response} res Express response.
     *
     * @return {Promise<void>}
     */
    protected createOne = async (req : Request, res : Response) : Promise<void> => {

        await this.beforeCreateOne(req, res);

        let data = req.body;
        let instance = new (this.getModel())(data);
        let saved = await instance.save();

        res.status(CREATED);
        res.contentType('application/json')
        res.json(saved);

        await this.afterCreateOne(req, res);

        return null;
    }


    /**
     * Create many instances of the model.
     *
     * @param {Request} req Express Request object
     * <pre>
     *  method: POST
     *  path: /&lt;plural_path&gt;
     *  params: (None)
     *  body:
     *      type: application/json
     *      paths:
     *          - path: 'instances'
     *            type: Array<object>
     *            description: array of instances following `model.schema`
     * </pre>
     *
     * @param {Response} res Express response
     * <pre>
     *  status:
     *      success:
     *          CREATED (200): document was successfully inserted.
     *      failure:
     *          NOT_ACCEPTABLE (403): JSON could not be parsed.
     *          INTERNAL_SERVER_ERROR (500): Something went wrong
     * </pre>
     *
     * @return null
     */
    protected createMany = async (req : Request, res : Response) : Promise<void> => {
        console.log(chalk.green(`CREATE_MANY ${stringify(req.body)}`));

        await this.beforeCreateMany(req, res);

        if (req.body.instances === undefined || req.body.instances === null) {
            await this.handleError(
                new Error(`\`instances\` expected, got ${stringify(Object.keys(req.body))}`),
                req, res, NOT_ACCEPTABLE
            );
            console.error(req.body);
            return;
        }

        let createdIds = new Array<string>();

        for (let inst of req.body.instances) {
            console.log(`Creating instance: ${stringify(inst)}`)
            let instance = new (this.getModel())(inst);
            try {
                await instance.save();
                createdIds.push(instance._id);
            } catch (err) {
                console.error(`Saving instance: ${err}`);
                await this.handleError(err, req, res, NOT_ACCEPTABLE);
                return null;
            }
        }

        res.status(CREATED)
            .json({"instances": createdIds});

        await this.afterCreateMany(req, res);

        return null;
    }


    /**
     * Create many instances of the model.
     *
     * @param {Request} req Express Request object
     * <pre>
     *  method: GET
     *  path: /&lt;plural_path&gt;
     *  params:
     *      - key: primaryKeyField() ("_id" unless overrridden)
     *        description: Primary ID of the object
     *        type: ObjectID hash.
     * </pre>
     *
     * @param {Resposne} res Express Response
     * <pre>
     *  status:
     *      success:
     *          OK (200): Object was found
     *      failure:
     *          NOT_ACCEPTABLE (403): Invalid ID
     *          NOT_FOUND (404): Object was not found with ID=:id
     *          INTERNAL_SERVER_ERROR (500): Something went wrong
     * </pre>
     *
     * @return {Primose<void>}
     */
    protected readOne = async (req : Request, res : Response) : Promise<void> => {
        await this.beforeReadOne(req, res);

        let id = req.params[this.getIdField()];
        let found: Document = null;
        try {
            found = await (this.getModel()).findById(id);
            console.debug(`Found: ${found}`);

            for (let entry in Object.entries(found)) {
                let [key, value] = entry;
                console.debug(`doExclude ${entry}`);
                if (!this.doExclude(req, res, key, value, found)) {
                    (found as any)[key] = value;
                }
            }

            if (!found) {
                res.sendStatus(NOT_FOUND);
                return null;
            } else {
                console.debug("FIND_ONE successful");
                res.status(OK).json(found);
            }

        } catch (err) {
            console.log(`Querying for ${id}: ${err}`);
            return await this.handleError(err, req, res, NOT_FOUND);
        }

        await this.afterReadOne(req, res);

        return null;
    }

    /**
     * Find instances of a model.
     *
     * Pattern: /&lt;plural_path&gt;?q=&lt;q&gt;&amp;page=&lt;page&gt;&amp;count=&lt;count&gt;
     * Params:
     * <pre>
     *  q:
     *      description: A mongoose filter. See `Model.findMany` for format.
     *      type: object.
     *  page (optional):
     *      description: Page number
     *      type: number
     *      default: 0
     *  count (optional):
     *      description: The number of items per page.
     *      type: number
     *      default: 10
     * </pre>
     *
     * @return A JSON object with the following properties:
     * <pre>
     *      "query":
     *          description: original query
     *          type: object
     *      "info":
     *          description: information for the given result.
     *          type: object
     *          properties:
     *              "total":
     *                  description: Total number of instances.
     *                  type: number
     *              "count":
     *                  description: Total number of instances for this page.
     *                  type: number
     *              "page":
     *                  description: page number returned.
     *                  type: number
     *              "pages":
     *                  description: total number of pages.
     *                  type: number
     *      "objects":
     *          description: objects within the specified range.
     *          type: array of objects.
     *      "previous":
     *          description: Path portion for the previous page. (or null if at beginning)
     *          type: string
     *      "next":
     *          description: Path portion for the next page. (or null if at end)
     *          type: string
     * </pre>
     */
    protected readMany = async (req : Request, res : Response) : Promise<void> => {
        console.debug(`READ_MANY`);

        await this.beforeReadMany(req, res);

        let q = req.query.q;
        let page = req.query.page || 0;
        let _count = req.query.count || 10;
        let fields = req.query.fields || this.getAllFields().join(" ");
        let sort = req.query.sort || this.getIdField();

        console.log(" -----");
        console.log("|");
        console.log(` READ_MANY q=${stringify(q)}`);
        console.log(` READ_MANY page=${page}`);
        console.log(` READ_MANY count=${_count}`);
        console.log(` READ_MANY sort=${sort}`);
        console.log(` READ_MANY fields=${fields}`);
        console.log("|______");

        let previousUrl = null;
        let nextUrl = null;

        if (page !== 0) {
            previousUrl = this.getPluralPath()
                + "/?q=" + q
                + "&page=" + String(page - 1)
                + "&count=" + String(_count)
                + "&fields=" + fields
                + "&sort=" + sort;
        }

        nextUrl = this.getPluralPath() + "/"
            + "?q=" + q
            + "&page=" + String(page+1)
            + "&count=" + String(_count)
            + "&fields=" + fields
            + "&sort=" + sort;


        let results = {
            request: {
                query: null as object,
                count: _count,
                page,
                sort
            },
            info: {
                total: 0,
                count: 0,
                page: 0,
                pages: 0,
            },
            objects: [] as any [],
            previous: previousUrl,
            next: nextUrl,
        };

        let filter = null;
        if (q) {
            try {
                filter = JSON.parse(q);
            } catch (err) {
                filter = null;
            }
        }

        if (!filter) {
            return await this.handleError(
                new Error("No query given."),
                req, res, NOT_ACCEPTABLE
            )
        }

        results.request.query = filter;

        let queryOptions = {
            limit: _count,
            skip: _count * page,
            sort,
        };

        try {
            results.info.total = await this.getModel().countDocuments(filter);
        } catch (err) {
            console.error(`Counting Documents: ${err}`);
            await this.handleError(err, req, res, INTERNAL_SERVER_ERROR);
        }
        try {
            results.objects = await (await this.getModel().find(filter, fields, queryOptions));
            results.objects = results.objects.map( (obj) => {
                Object.entries(obj).forEach((entry) => {
                    let [key, value] = entry;
                    if (this.doExclude(req, res, key, value, obj))
                        return;
                    (obj as any)[key] = value;
                });
                return obj;
            });
            console.log(`Retrieved ${results.objects.length} objects`);
        } catch (err) {
            console.error(`Finding Documents: ${err}`);
            await this.handleError(err, req, res, INTERNAL_SERVER_ERROR);
            return null;
        }
        results.info.count = results.objects.length || 0;
        results.info.page = page;
        results.info.pages = Math.floor(results.info.total / _count);

        // There aren't any more. This is the last page.
        results.next = (results.info.count < _count)? results.next : null;

        console.log(`READ_MANY returning results ${stringify(results)}`);

        res.status(OK).json(results);

        await this.afterReadMany(req, res);
    }

    /**
     * Update an instnace of the model.
     *
     * @param {Request} req Express Request object
     * <pre>
     *  method: PATCH
     *  path: /&lt;path&gt;/:&lt;primary_id_field&gt;
     *  params:
     *      - key: <primary_id_field>, typically "_id"
     *        type: ObjectID
     *        description: The ID to delete.
     *  body:
     *      type: application/json
     *      description: Filter for the object to delete (same as `Model.deleteOne`)
     * </pre>
     *
     * @param {Response} res Express response
     * <pre>
     *  status:
     *      success:
     *          OK (200): document was deleted
     *      failure:
     *          INTERNAL_SERVER_ERROR (500): Something went wrong
     * </pre>
     *
     * @return null
     */
    protected updateOne = async (req : Request, res : Response) : Promise<void> => {

        let id = req.params._id
        let data = req.body;

        await this.beforeUpdateOne(req, res);

        try {
            await this.getModel().updateOne({"_id": new ObjectId(id)}, data);
            res.status(OK).json({"_id": id});
        } catch (err) {
            console.log(`Updating ${id} with ${data}`);
            await this.handleError(err, req, res, NOT_ACCEPTABLE);
            return null;
        }

        await this.afterUpdateOne(req, res);
        return null;
    }

    /**
     * Delete an instnace of the model.
     *
     * @param req Express Request object
     * <pre>
     *  method: DELETE
     *  path: /&lt;path&gt;/:&lt;primary_id_field&gt;
     *  params:
     *      - key: <primary_id_field>, typically "_id"
     *        type: ObjectID
     *        description: The ID to delete.
     * </pre>
     *
     * @param res Express response
     * <pre>
     *  status:
     *      success:
     *          OK (200): document was deleted
     *      failure:
     *          INTERNAL_SERVER_ERROR (500): Something went wrong
     * </pre>
     *
     * @return null
     */
    protected deleteOne = async (req : Request, res : Response) : Promise<void> => {

        let id = req.params._id

        await this.beforeDeleteOne(req, res);

        let _idField = this.getIdField() as string;

        try {
            await this.getModel().deleteOne({_idField: new ObjectId(id)}).exec();
            res.sendStatus(OK);
        } catch (err) {
            console.error(`While deleting ${id}`);
            await this.handleError(err, req, res);
            return null;
        }

        await this.afterDeleteOne(req, res);
        return null;
    }

    /**
     * #########################
     *      HELPER METHODS
     * #########################
     */

    /**
     * Get the model of the Ningilin.
     *
     * @description This should probably not be overridden unless absolutely
     * necessary.
     *
     * @return {Model}
     */
    public getModel = () : Model<Document> => {
        return this._model;
    }

    /**
     * Get the Mongoose model associated with the NIngilin.
     */
    public get model () : Model<Document> { return this._model; }

    /**
     * If this hook returns `true`, then the field with key `key` will be
     * ommitted from the object of READ_ONE and READ_MANY.
     * 
     * @param req {Request} Express request.
     * @param res {Response} Express response.
     * @param key {string} The current key being processed.
     * @param value {any} The current value being processed.
     * @param data {object} The main object being processed. Optional
     * 
     * @return {boolean} `true` if the field should be excluded, `false` otherwise.
     */
    public doExclude = (req : Request, res : Response, key : any, value : any, data?: object) : boolean => { return false; }


    /**
     * Get the name of the 'id' field for Mongoose.
     *
     * @return {string} The ID field (default is `"_id"`).
     */
    public getIdField = () : string => {
        return "_id";
    }

    private HOOKS = {
        getPath: this.getPath,
        getPluralPath: this.getPluralPath,
        getModel: this.getModel,
        getIdField: this.getIdField,
        doExclude: this.doExclude,

        beforeConnect: this.beforeConnect,
        afterConnect: this.afterConnect,

        beforeDisconnect: this.beforeDisconnect,
        afterDisconnect: this.afterDisconnect,

        beforeCreateOne: this.beforeCreateOne,
        createOne: this.createOne,
        afterCreateOne: this.afterCreateOne,

        beforeCreateMany: this.beforeCreateMany,
        createMany: this.createMany,
        afterCreateMany: this.afterCreateMany,

        beforeReadOne: this.beforeReadOne,
        readOne: this.readOne,
        afterReadOne: this.afterReadOne,

        beforeReadMany: this.beforeReadMany,
        readMany: this.readMany,
        afterReadMany: this.afterReadMany,

        beforeUpdateOne: this.beforeUpdateOne,
        updateOne: this.updateOne,
        afterUpdateOne: this.afterUpdateOne,

        beforeDeleteOne: this.beforeDeleteOne,
        deleteOne: this.deleteOne,
        afterDeleteOne: this.afterDeleteOne,

        handleError: this.handleError,
    }


    protected _preHooks: Hook[];
    protected _postHooks: Hook[];
    protected _hooks: { [index: string]: Callable; };


    /**
     * Construct a new Ningilin Model.
     * @constructor
     * @param name Name of the collection
     * @param schema Mongoose Schema associated with the collection, either
     *  object or `Schema`. If it's an object, then it will be converted to a
     *  Schema. This is so that `mongoose` will not need to be imported.
     * @param hooks Optional hooks to override for the Ningilin
     * @param path Path that will be generated for the router (default is lowercase `name`)
     * @param preHooks List of hooks that will be called before *any* operation
     * @param postHooks List of hooks that will be called after *any* operation
     * @param collection Collection for the model (not used)
     * @param skipInit Skip initialization (not used)
     */
    constructor (name: string, schema: {[key: string] : any} | Schema,
                hooks?: {[index: string]: Callable},
                path? : string,
                preHooks? : Hook[],
                postHooks ? : Hook[]) {
        this._name = name;
        this._path = path || name.toLowerCase();
        // schema.set("collection", (collection || this._name));
        if (!(schema instanceof Schema))
            schema = new Schema(schema as {});
        this._model = model(this._name, schema as Schema);
        this._preHooks = preHooks;
        this._postHooks = postHooks;

        const VALID_KEYS = Object.keys(this.HOOKS);

        this._hooks = hooks || {};

        Object.keys(hooks || {}).forEach((key : string, index: number, array: string []) : void => {
            if (VALID_KEYS.indexOf(key) < 0) {
                throw new Error(`'${key}' is not a valid key. Expected one of ${VALID_KEYS}`);
            }
            console.debug(`Assigning ${key}`);
            (this as {[index: string]: any})[key] = hooks[key];
            return;
        });
    }

    private ACTION_HOOKS : Map<CrudAction, string> = new Map(
        [
            [CrudAction.CREATE_ONE, "createOne"],
        ]
    );

    /**
     * Get a mapping of actions to paths.
     *
     * @return a PathReference type where action => path
     */
    public get paths () : {[key in Action] : string} {
        let idField = ((this._hooks.getIdField as () => string | null) || this.getIdField)();
        return {
            createOne: this.getPath(),
            createMany: this.getPluralPath(),
            readOne: this.getPath() + "/:" + idField,
            readMany: this.getPluralPath(),
            updateOne: this.getPath() + "/:" + idField,
            updateMany: this.getPluralPath(),
            deleteOne: this.getPath() + "/:" + idField,
            deleteMany: this.getPluralPath(),
        };
    }

    /**
     * Get a mapping of actions to paths.
     *
     * @return a PathReference type where action => path
     */
    private getRouterMethod (router : Router) {
        let idField = ((this._hooks.getIdField as () => string | null) || this.getIdField)();
        return {
            createOne: router.post,
            createMany: router.post,
            readOne: router.get,
            readMany: router.get,
            updateOne: router.patch,
            updateMany: router.patch,
            deleteOne: router.delete,
            deleteMany: router.delete,
        };
    }

    /**
     * Express methods associated with an action.
     */
    public get methods () : {[key in Action] : string} {
        return {
            createOne: 'post',
            createMany: 'post',
            readOne: 'get',
            readMany: 'get',
            updateOne: 'patch',
            updateMany: 'patch',
            deleteOne: 'delete',
            deleteMany: 'delete',
        };
    }

    private getCallback = (action : Action) : ExpressHook => {
        let chosenHook : Hook;

        chosenHook = ((this as any)[action as string] as Hook);

        return async (req : Request, res : Response, next : NextFunc) : Promise<any> => {
            console.debug(chalk.greenBright(`${req.method} ${req.path}`));
            console.debug(`With data: ${stringify(req.body)}`);

            if (chosenHook == null) {
                console.error(`No hook found for ${req.method} ${req.path}`);
                return this.handleError(new Error("Invalid URL"), req, res, NOT_FOUND);
            }

            try {
                await this.connect(req, res);
            } catch (err) {
                await this.handleError(err, req, res);
                next();
                return;
            }

            (this._preHooks || []).forEach( async(hook : Hook) => {
                await hook(req, res);
            });


            try {
                await (chosenHook as Hook)(req, res);
            } catch (err) {
                console.error(`Executing hook: ${err}`);
                this.handleError(err, req, res);
                return;
            }


            (this._postHooks || []).forEach( async(hook : Hook) => {
                await hook(req, res);
            });

            try {
                await this.disconnect(req, res);
            } catch (err) {
                await this.handleError(err, req, res, INTERNAL_SERVER_ERROR);
            }
            next();

            return null;
        }
    }

    /**
     * Thanks to https://stackoverflow.com/a/7356528
     */
    private isFunction = (functionToCheck: any) : boolean => {
        return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
    }

    /**
     * Get a list of all the paths (including the method) of this route.
     *
     * @return Array<string> in the format `&lt;method&gt; &lt;path&gt;`
     */
    public getPaths () : string[] {
        let paths = new Array<string>();
        this.getRouter().stack.forEach( (r) => {
            if (r.route) {
                Object.keys(r.route.methods).forEach( (m : string) => {
                    if (r.route && r.route.path && r.route.methods[m]) {
                        paths.push(m.toUpperCase() + " " + r.route.path);
                    }
                });
            }
        });
        return paths;
    }

    /**
     * Get the router for express.
     *
     * @return {Router}
     */
    public getRouter () : Router {
        let router = Router();
        router.use(json());
        router.use((req : Request, res : Response, next: (() => any)) : void => {
            console.log(chalk.blue(`--> ${req.method} ${req.path}`));
            next();
        });

        console.log(chalk.redBright("here"));

        // Assign a callback for each of the actions.
        for (let action in Action) {
            if (action == null) continue;
            let a = (Action as any)[action];
            let m = (this.methods as any)[a];
            let p = (this as any).paths[a];
            router = ((router as any)[m] as typeof router.get)(p, this.getCallback(a));
        }

        // Special _schema hook
        router.get('/_schema' + this.getPath(), (req : Request, res : Response, next : () => void) : void => {
            res.status(OK).json(this._model.schema.obj).send();
            next();
        });

        return router;
    }

    /**
     * Utility method to get all fields for the model.
     *
     * @param {Array<string>} exclude Fields to exclude.
     * @defult exclude ["__v"]
     *
     * @return {String<array>} array of fields for the model.
     */
    public getAllFields = (exclude = ["__v"]) : string [] => {
        let keys = Object.keys(this.getModel().schema.obj);
        return keys.filter( (value, index, array) => {
            console.debug(`is ${value} in ${keys} ?`);
            return exclude.indexOf(value) < 0;
        });
    }

    public getAllHooks = () : string [] => {
        return Object.keys(this.HOOKS);
    }
}