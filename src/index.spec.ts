import axios, { AxiosInstance } from "axios";
import chai = require('chai');
const chaiQuantifiers = require('chai-quantifiers');
import chaiHttp = require('chai-http');
const chaiJsonEqual = require('chai-json-equal');
const chaiLike = require('chai-like');
const assert = require('chai').assert;
import dotenv from 'dotenv';
import {connect as mongoose_connect, disconnect as mongoose_disconnect, Document} from "mongoose";
import express, {IRoute} from "express";
import { Schema, set as mongoose_set } from "mongoose";
import faker from "faker";
import { CREATED, OK, NOT_ACCEPTABLE, NOT_FOUND } from "http-status-codes";
import stringify = require("json-stringify-safe");
import shuffle from "shuffle-array";
// import {beforeEach, afterEach, it} from "ts-mocha";
import {describe, beforeEach, afterEach, it} from "mocha";

// Ningilin;
import { NingilinModel } from ".";
import chalk = require("chalk");

const { expect } = chai;

chai.use(chaiHttp);
chai.use(chaiJsonEqual);
chai.use(chaiLike);
chai.use(chaiQuantifiers);

mongoose_set('bufferCommands', false);

describe('Ningilin Functionality', () => {
    let port : number;
    let app : any;
    let server : any;
    let httpClient : AxiosInstance;

    const peopleSchema = new Schema({
        first_name : {
            type: String,
            required: true,
        },
        last_name : {
            type: String,
            required: true,
        },
        age : Number,
    });

    // Helper functions

    let PersonNingilin = new NingilinModel("person", peopleSchema, {
            getPluralPath: () => { return "/people" },
        }
    );

    function generatePerson(commonFirstName? : string) : {[index: string]: any} {
        return {
            'first_name' : faker.name.firstName(),
            'last_name': faker.name.lastName(),
            'age': faker.random.number(20)
        };
    }

    const destroyDatabase = async () : Promise<void> => {
        let opts = process.env.MONGODB_OPTIONS || {};
        let mongoose = await mongoose_connect(process.env.MONGODB_URL, opts);
        await mongoose.connection.dropDatabase();
        console.debug(`Database dropped`);
        await mongoose_disconnect();
        return null;
    };

    const prepopulatePeople = async (commonName? : string, n=10, popularity?: number) : Promise<Document []> => {
        popularity = popularity || Math.random();
        let Model = PersonNingilin.getModel();
        let result = [];
        let opts = process.env.MONGODB_OPTIONS || {};
        await mongoose_connect(process.env.MONGODB_URL, opts);
        for (let i = 0; i < n; ++i) {
            let p = generatePerson();
            if (i < Math.round((n * popularity)))
                p.first_name = commonName;
            let instance = new Model(p);
            result.push(await instance.save());
        }
        await mongoose_disconnect();
        shuffle(result);
        return result;
    }

    // Setup

    beforeEach(() => {
        dotenv.config();

        if (!PersonNingilin) {
            throw new Error(`Could not create Ningilin`);
        }

        app = express();
        app.use(express.json());
        app.use(PersonNingilin.getRouter());
        port = (Number(process.env.PORT)) || 5002;

        server = app.listen(port);

        if (!server) {
            throw new Error(`Could not create server`);
        }

        httpClient = axios.create({
            baseURL: `http://localhost:${port}`,
        });

        return destroyDatabase();
    });

    // Teardown

    afterEach(() => {
        server.close();
    });

    // TODO : Initial connect with bad connection.

    // The actual tests

    it("Router contains appropriate routes.", (done) => {
        let testNingilin = new NingilinModel("Person", peopleSchema);
        let router = testNingilin.getRouter();
        console.log(testNingilin.getPaths());
        expect(router.stack).to.containOne(r => r.route && r.route.path && r.route.path == '/person');
        expect(router.stack).to.containOne(r => r.route && r.route.path && r.route.path == '/persons');
        done();
    })

    it("We can successfully override the hooks.", (done) => {
        let testNingilin = new NingilinModel("TestNingilin", peopleSchema, {
            "getPath": (<Callable> () : string => { return "/oneNingilin"; }),
            "getPluralPath": (<Callable> () : string => { return "/twoNingilin"; })
        });
        assert.equal(testNingilin.getPath(), "/oneNingilin");
        assert.equal(testNingilin.getPluralPath(), "/twoNingilin");
        done();
    });

    it("If we pass in `null` for optional arguments, nothing bad happens", (done) => {
        assert.doesNotThrow(() => {
            let testNingilin = new NingilinModel("Test", peopleSchema, null, null, null, null);
        }, Error);
        done();
    });

    it("Error is thrown if Ningilin is created with invalid hook", (done) => {
        assert.throws( () => {
            // tslint:disable-next-line: no-unused-expression
            new NingilinModel("Person", peopleSchema, {
                "my_hook": (<Callable> () : string => { return "yes"; })
            });
            done();
        });
        done();
    });

    it("Create One Successful", (done) => {
        // setTimeout(() => { console.error(`timeout reached. so sad`)}, 30000);
        chai.request(server)
            .post(PersonNingilin.getPath())
            .type("application/json")
            .send(generatePerson())
            .end((err, res) => {
                if (err) console.error(`ERROR: ${err}`);
                console.log(chalk.bgBlueBright(JSON.stringify(res)));
                expect(res).to.have.status(CREATED);
                assert.property(res.body, 'first_name')
                assert.property(res.body, 'last_name')
                assert.property(res.body, 'age')
                done();
            });
    });

    it("Create Many Successful", (done) => {
        let createdPeople = [];
        for (let i = 0; i < 10; ++i)
            createdPeople.push(generatePerson());
        let path = PersonNingilin.getPluralPath() + "/";
        let data = {"instances": createdPeople};
        console.debug(`POSTing instances: ${stringify(data)}`);
        chai.request(server)
            .post(path)
            .send(data)
            .end((err, res) => {
                if (err) console.error(`ERROR: ${err}`);
                console.log(`BODY: ${stringify(res.body)}`);
                expect(res).to.have.status(CREATED);
                assert.property(res.body, "instances");
                assert.equal(res.body.instances.length, 10);
                done();
            });
    });

    it("Create Many Unsuccessful", (done) => {
        // setTimeout(() => { console.error(`timeout reached. so sad`)}, 30000);
        chai.request(server)
            .post(PersonNingilin.getPluralPath() + "/")
            .type("application/json")
            .send({})
            .end((err, res) => {
                if (err) console.error(`ERROR: ${err}`);
                expect(res).to.have.status(NOT_ACCEPTABLE);
                done();
            });
    });

    it("Read One Successful", (done) => {
        // setTimeout(() => { console.error(`timeout reached. so sad`)}, 30000);
        let name = faker.name.firstName();
        prepopulatePeople(name).then( (people : Document[]) => {
            chai.request(server)
                .get(PersonNingilin.getPath() + "/" + people[0]._id)
                .type("application/json")
                .end((err, res) => {
                    if (err) console.error(`ERROR: ${err}`);
                    expect(res).to.have.status(OK);
                    done();
                })
            }
        ).catch((err : Error) => {
            console.error(err);
        });
    });

    it("Read One Unsuccessful", (done) => {
        // setTimeout(() => { console.error(`timeout reached. so sad`)}, 30000);
        let name = faker.name.firstName();
        prepopulatePeople(name).then( (people : Document[]) => {
            chai.request(server)
                .get(PersonNingilin.getPath() + "/not_a_valid_id")
                .type("application/json")
                .end((err, res) => {
                    if (err) throw err;
                    expect(res).to.have.status(NOT_FOUND);
                    done();
                });
        }).catch((err : Error) => {
            done();
            console.error(err);
        });
    });

    /////////////////////////
    // READ MANY
    /////////////////////////

    it("Read Many Successful", (done) => {
        // setTimeout(() => { console.error(`timeout reached. so sad`)}, 30000);
        let name = faker.name.firstName();
        let q = {"first_name": name};
        prepopulatePeople(name, 100, 0.1).then( (people : Document[]) => {
            chai.request(server)
                .get(PersonNingilin.getPluralPath() + "/\?q=" + stringify(q))
                .type("application/json")
                .end((err, res) => {
                    if (err) console.error(`ERROR: ${err}`);
                    expect(res).to.have.status(OK);
                    console.log(`READ_MANY response body = ${JSON.stringify(res.body)}`);
                    // Initial properties
                    assert.property(res.body, "request");
                    assert.property(res.body.request, "query");
                    assert.equal(res.body.request.page, 0);
                    assert.property(res.body.request, "query");
                    assert.isNotNull(res.body.request.query);
                    assert.deepEqual(res.body.request.query, q);
                    assert.property(res.body, "info");
                    assert.property(res.body.info, "total");
                    assert.property(res.body.info, "count");
                    assert.property(res.body.info, "page");
                    assert.property(res.body.info, "pages");
                    assert.property(res.body, "objects");
                    assert.property(res.body, "next");
                    assert.property(res.body, "previous");
                    // Values
                    assert.equal(res.body.info.count, 10);
                    assert.isAtLeast(res.body.info.total, 10);
                    assert.equal(res.body.info.page, 0);
                    assert.equal(res.body.info.pages, 1);
                    assert.equal(res.body.objects.length, 10);
                    // Ensure all objects have the common first name.
                    res.body.objects.forEach((obj : {[index: string] : any}, index : number, array: object []) => {
                        assert.deepEqual(obj.first_name, name, `Index ${index} isn't "${name}"`);
                    });
                    done(err);
                });
            }
        ).catch((err : Error) => {
            console.error(err);
            done();
        });
    });

    it("Read Many Successful (Paginated)", (done) => {
        // setTimeout(() => { console.error(`timeout reached. so sad`)}, 30000);
        let name = faker.name.firstName();
        let q = {"first_name": name};
        let page = 1;
        prepopulatePeople(name, 100, 0.5).then( (people : Document[]) => {
            chai.request(server)
                .get(PersonNingilin.getPluralPath()
                     + "/?q=" + stringify(q)
                     + "&page=" + String(page))
                .type("application/json")
                .end((err, res) => {
                    if (err) console.error(`ERROR: ${err}`);
                    expect(res).to.have.status(OK);
                    // Initial properties
                    assert.property(res.body, "request");
                    assert.property(res.body.request, "query");
                    assert.equal(res.body.request.page, 1);
                    assert.property(res.body.request, "query");
                    assert.deepEqual(res.body.request.query, q);
                    assert.property(res.body, "info");
                    assert.property(res.body.info, "total");
                    assert.property(res.body.info, "count");
                    assert.property(res.body.info, "page");
                    assert.property(res.body.info, "pages");
                    assert.property(res.body, "objects");
                    assert.property(res.body, "next");
                    assert.property(res.body, "previous");
                    // Values
                    assert.isAtLeast(res.body.info.count, 10);
                    assert.equal(res.body.info.page, 1);
                    assert.isAtLeast(res.body.objects.length, 10);
                    // Ensure all objects have the common first name.
                    res.body.objects.forEach((obj : {[index: string] : any}, index : number, array: object []) => {
                        assert.deepEqual(obj.first_name, name, `Index ${index} isn't "${name}"`);
                    });
                    done(err);
                });
            }).catch((err : Error) => {
                console.error(err);
                done(err);
            });
    });

    it("Update One", (done) => {
        let name = faker.name.firstName();
        let newName = faker.name.firstName();
        prepopulatePeople(name).then( (people : Document[]) => {
            let chosenPerson = people[0];
            let pid = people[0]._id;
            let oldName = chosenPerson.get('first_name');
            console.debug(`Changing ${oldName}'s name (id=${pid}) to ${newName}`)
            chai.request(server)
                .patch(PersonNingilin.getPath() + `/${pid}`)
                .type("application/json")
                .send({"first_name": newName})
                .end((err, res) => {
                    if (err) console.error(err);
                    expect(res).to.have.status(OK);
                    // Get the person to verify it's bee updated.
                    axios.get(PersonNingilin.getPath() + `/${pid}`).then((res) => {
                        console.debug(`Verifying ${res.data} name=${newName}`);
                        assert.equal(res.data.first_name, newName);
                        done();
                    }).catch( (err) => {
                        console.error(`Axios error: ${err.message}`);
                        done();
                    });
                });
            }
        ).catch((err : Error) => {
            console.error(err);
        });
    });

    // it("Update One - Fail with NOT_ACCEPTABLE data.", (done) => {
    //     let name = faker.name.firstName();
    //     let newName = faker.name.firstName();
    //     prepopulatePeople(name).then( (people : Document[]) => {
    //         let chosenPerson = people[0];
    //         let pid = people[0]._id;
    //         let oldName = chosenPerson.get('first_name');
    //         console.debug(`Changing ${oldName}'s name (id=${pid}) to ${newName}`)
    //         chai.request(server)
    //             .patch(PersonNingilin.getPath() + `/${pid}`)
    //             .type("application/json")
    //             .send({"_id": 4})
    //             .end((err, res) => {
    //                 if (err) console.error(err);
    //                 expect(res).to.have.status(NOT_ACCEPTABLE);
    //                 done();
    //             });
    //         }
    //     ).catch((err : Error) => {
    //         console.error(err);
    //         done();
    //     });
    // });

    it("Delete One.", (done) => {
        let name = faker.name.firstName();
        prepopulatePeople(name).then( (people : Document[]) => {
            let pid = people[0]._id;
            chai.request(server)
                .delete(PersonNingilin.getPath() + `/${pid}`)
                .then((res) => {
                    expect(res).to.have.status(OK);
                    axios.get(PersonNingilin.getPath() + "/" + pid).then( (resp : any) => {
                        expect(resp).to.have.statusCode(NOT_FOUND);
                        done();
                    }).catch( (err : Error) => {
                        console.error(`Was it deleted? ${err}`);
                        done();
                    });
                }).catch((err : Error) => {
                    console.error(err);
                    done();
                });
        });
    });

});
