import { Router } from "express";

import {Schema, Model, Document} from "mongoose";

import {Request, Response} from "express";


export declare enum Action {
    CREATE_ONE = "createOne",
    CREATE_MANY = "createMany",
    READ_ONE = "readOne",
    READ_MANY = "readMany",
    UPDATE_ONE = "updateOne",
    UPDATE_MANY = "updateMany",
    DELETE_ONE = "deleteOne",
    DELETE_MANY = "deleteMany",
}

export declare type Hook = (req : Request, res : Response) => Promise<void>;
export declare type InfoHook = () => string;
export declare type ErrorHandler = (err : Error, req : Request, res : Response) => Promise<void>;
export declare type Callable = (Hook | InfoHook | ErrorHandler);

export declare class HookMap {
    constructor(hooks: Array<Callable>);
}

export type PathReference = {
    [key in Action]: string
};

export type HookOverrides = {
    beforeConnect?: Hook,
    afterConnect?: Hook,
    beforeCreateOne?: Hook,
    afterCreateOne?: Hook,
    beforeCreateMany?: Hook,
    afterCreateMany?: Hook,

    beforeReadOne?: Hook,
    afterReadOne?: Hook,

    beforeUpdateOne?: Hook,
    afterUpdateOne?: Hook,

    beforeDeleteOne?: Hook,
    afterDeleteOne?: Hook,

    beforeDeleteMany?: Hook,
    afterDeleteMany?: Hook,

    beforeReadMany?: Hook,
    afterReadMany?: Hook,

    beforeDisconnect?: Hook,
    afterDisconnect?: Hook,

     getPath? : InfoHook,
     getPluralPath? : InfoHook;

     getConnection? : InfoHook;
}

export class NingilinModel {
    getAllHooks() : string [];
    getAllFields(exclude : string []) : string [];
    getRouter() : Router;
    getPaths() : string [];
    paths : {[key in Action] : string};
    methods: {[key in Action] : string};
    model : Model<Document>;
    constructor(name: string, schema: {[key: string] : any}  | Schema,
        hooks?: HookOverrides,
        path? : string,
        preHooks? : Hook[],
        postHooks ? : Hook[],
        collection ? : string,
        skipInit ? : boolean);
}